# Maze_AbstractFactory
Abstract factory example from GoF Design Patterns.
Implements a Maze game.

## Definition
Provides an interface for creating families of related or dependent objects without specifying their concrete classes.

## Applicability
- when a system should be independent of how it's products are created, composed and represented
- When a system should be configured with one of the multiple families of products.
- When a family of related products is designed to be used together and we need to enforce this constraint.

## Participants
1) AbstractFactory: Declares an interface for operations that create abstract products objects.
2) ConcreteFactory: Implements the operations to create concrete product objects.
3) AbstractProduct: Declares an interface for a type of products.
4) ConcreteProduct: Implements the AbstractProduct interface.
5) Client: Uses only interfaces declared by AbstractFactory and AbstractProduct.

## Implementation:
- Parameterized factories: Adding a parameter to the factory can control what objects to create. This is more flexible but less safe design because the client will always get an AbstractProduct. This might mean downcasting the object received by client, which is risky since it might or might not work at runtime.
