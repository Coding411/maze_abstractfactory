import factory.Level0MazeFactory;
import factory.Level1MazeFactory;
import factory.MazeAbstractFactory;

public class Client {

    public static void main(String[]args) {

        MazeAbstractFactory mazeFactory = new Level0MazeFactory();
        mazeFactory.createMaze();

        mazeFactory = new Level1MazeFactory();
        mazeFactory.createMaze();
    }
}
