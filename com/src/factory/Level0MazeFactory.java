package factory;

import factory.door.Door;
import factory.door.UnlockedDoor;
import factory.mazeRoom.Room;
import factory.mazeRoom.SimpleRoom;

public class Level0MazeFactory implements  MazeAbstractFactory{

    @Override
    public void createMaze() {

        Room r1 = new SimpleRoom();
        Room r2 = new SimpleRoom();
        Door door = new UnlockedDoor();
        System.out.println("Created Level1 maze");
    }
}
