package factory;

import factory.door.Door;
import factory.door.LockedDoor;
import factory.door.UnlockedDoor;
import factory.mazeRoom.Room;
import factory.mazeRoom.SimpleRoom;
import factory.mazeRoom.TrapRoom;

public class Level1MazeFactory implements MazeAbstractFactory{

    @Override
    public void createMaze() {

        Room r1 = new TrapRoom();
        Room r2 = new SimpleRoom();
        Door d1 = new LockedDoor();
        Door d2 = new UnlockedDoor();
        System.out.println("Created Level1 maze");
    }
}
