package factory.door;

public interface Door {

    void openDoor();
}
