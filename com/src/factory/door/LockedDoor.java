package factory.door;

public class LockedDoor implements Door{

    public LockedDoor() {
        System.out.println("Created Locked factory.door.Door");
    }

    @Override
    public void openDoor() {
        System.out.println("Locked door has been opened.");
    }
}
