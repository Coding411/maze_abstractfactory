package factory.door;

public class UnlockedDoor implements Door{
    public UnlockedDoor() {
        System.out.println("Created Unlocked factory.door.Door");
    }

    @Override
    public void openDoor() {
        System.out.println("Unlocked door has been opened.");
    }
}
